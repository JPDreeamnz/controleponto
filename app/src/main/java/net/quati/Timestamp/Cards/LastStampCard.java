package net.quati.Timestamp.Cards;

import android.content.Context;
import android.media.Image;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import net.quati.Datasource.TimeStampDatasource;
import net.quati.Model.TimeStamp;
import net.quati.Timestamp.R;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardHeader;
import it.gmariotti.cardslib.library.prototypes.CardWithList;

/**
 * Created by João Pedro R. Carvalho on 01/12/2015.
 */
public class LastStampCard extends CardWithList {

    private String Title;

    public LastStampCard(Context context, String Title) {
        super(context);
        this.Title = Title;
    }

    @Override
    protected CardHeader initCardHeader() {
        CardHeader header = new CardHeader(getContext());
        header.setTitle(this.Title);
        return header;
    }

    @Override
    protected void initCard() {

    }

    @Override
    protected List<ListObject> initChildren() {
        List<ListObject> stampList = new ArrayList<ListObject>();

        TimeStampDatasource dtTimeStamp = new TimeStampDatasource(getContext());
        List<TimeStamp> timeStampList = dtTimeStamp.findAll();
        for (TimeStamp item : timeStampList) {
            stampList.add(new TimeStampCard(item));
        }

        return stampList.size() > 0 ? stampList : null;
    }

    @Override
    public View setupChildView(int childPosition, ListObject object, View convertView, ViewGroup parent) {
        TextView registerDateTime = (TextView) convertView.findViewById(R.id.registerDateTime);
        ImageView stampType = (ImageView) convertView.findViewById(R.id.stampType);

        TimeStampCard timeStamp = (TimeStampCard)object;
        registerDateTime.setText(timeStamp.RegisterDateTime.toString());
        if (timeStamp.StampType == 0)
            stampType.setImageResource(android.R.drawable.presence_online);
        else
            stampType.setImageResource(android.R.drawable.presence_busy);

        return convertView;
    }

    @Override
    public int getChildLayoutId() {
        return R.layout.content_card_stampitem;
    }

    public void updateItems() {
        getLinearListAdapter().addAll(initChildren());
    }
}

class TimeStampCard extends TimeStamp implements CardWithList.ListObject
{
    public TimeStampCard(TimeStamp object)
    {
        this.TimeStampID = object.TimeStampID;
        this.OriginalTimeStampID = object.OriginalTimeStampID;
        this.RegisterDateTime = object.RegisterDateTime;
        this.StampType = object.StampType;
        this.Status = object.Status;
        this.UpdateDateTime = object.UpdateDateTime;
    }

    @Override
    public String getObjectId() {
        return null;
    }

    @Override
    public Card getParentCard() {
        return null;
    }

    @Override
    public void setOnItemClickListener(CardWithList.OnItemClickListener onItemClickListener) {

    }

    @Override
    public CardWithList.OnItemClickListener getOnItemClickListener() {
        return null;
    }

    @Override
    public boolean isSwipeable() {
        return false;
    }

    @Override
    public void setSwipeable(boolean isSwipeable) { }

    @Override
    public CardWithList.OnItemSwipeListener getOnItemSwipeListener() {
        return null;
    }

    @Override
    public void setOnItemSwipeListener(CardWithList.OnItemSwipeListener onSwipeListener) {

    }
}
