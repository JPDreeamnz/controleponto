package net.quati.Timestamp.Activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.DigitalClock;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextClock;

import net.quati.Business.TimeStampBusiness;
import net.quati.Datasource.DatabaseAccess;
import net.quati.Model.TimeStamp;
import net.quati.Timestamp.Cards.LastStampCard;
import net.quati.Timestamp.R;

import it.gmariotti.cardslib.library.view.CardViewNative;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //TODO: Criar o botão que limpa o banco de dados
        RelativeLayout layoutContent = (RelativeLayout) findViewById(R.id.layout_content);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Insert a clock based on android's version
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1){
            TextClock textClock = new TextClock(this);
            textClock.setAllCaps(true);
            textClock.getTimeZone();
            textClock.setFormat24Hour("HH:mm");
            textClock.setTextColor(ContextCompat.getColor(this, R.color.white));
            textClock.setTextSize(100);
            textClock.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            textClock.setShadowLayer(2, 0, 2, ContextCompat.getColor(this, R.color.black));
            textClock.setLayoutParams(new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT
                    )
            );

            layoutContent.addView(textClock);
        }
        else {
            DigitalClock digitalClock = new DigitalClock(this);
            digitalClock.setAllCaps(true);
            digitalClock.setTextColor(getResources().getColor(R.color.white));
            digitalClock.setTextSize(100);
            digitalClock.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            digitalClock.setShadowLayer(2, 0, 2, getResources().getColor(R.color.black));
            digitalClock.setLayoutParams(new LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT
                    )
            );

            layoutContent.addView(digitalClock);
        }

        // Setting the Last Stamps Card List
        final LastStampCard lastStampCard = new LastStampCard(this, "Last Stamps");
        lastStampCard.init();
        CardViewNative cardViewLastStamp = (CardViewNative) findViewById(R.id.listLastStamps);
        cardViewLastStamp.setCard(lastStampCard);

        // Insert a Floation action button
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.newTimeStamp);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimeStampBusiness business = new TimeStampBusiness(HomeActivity.this);
                TimeStamp newStamp = business.CreateStamp();
                lastStampCard.updateItems();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.resetar_db) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(HomeActivity.this);
            dialog.setMessage(R.string.reset_db);
            dialog.setCancelable(true);
            dialog.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    DatabaseAccess access = new DatabaseAccess(HomeActivity.this);

                    dialog.cancel();

                    AlertDialog.Builder dialogReset = new AlertDialog.Builder(HomeActivity.this);
                    dialogReset.setTitle(R.string.alert);
                    dialogReset.setCancelable(true);
                    if (access.ResetDatabase()) {
                        dialogReset.setMessage(R.string.reset_success);
                    } else {
                        dialogReset.setMessage(R.string.reset_error);
                    }

                    dialogReset.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    AlertDialog alertReset = dialogReset.create();
                    alertReset.show();
                }
            });
            dialog.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            AlertDialog alert = dialog.create();
            alert.show();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
