package net.quati.Business;

import android.content.Context;

import net.quati.Datasource.TimeStampDatasource;
import net.quati.Model.TimeStamp;

import java.util.Date;

/**
 * Created by João Pedro R. Carvalho on 03/12/2015.
 */
public class TimeStampBusiness {
    private TimeStampDatasource dsTimestamp;

    public TimeStampBusiness(Context context){
        dsTimestamp = new TimeStampDatasource(context);
    }

    public TimeStamp CreateStamp(){
        TimeStamp lastStamp = dsTimestamp.getLastTimeStamp();

        TimeStamp newStamp = new TimeStamp();
        newStamp.RegisterDateTime = new Date();
        newStamp.StampType =
                (lastStamp != null && lastStamp.StampType == 1) ? 0 : 1;
        newStamp.OriginalTimeStampID =
                (lastStamp != null && lastStamp.StampType == 1) ? lastStamp.TimeStampID : null;
        newStamp.Status = true;

        return dsTimestamp.Insert(newStamp);
    }
}
