package net.quati.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by João Pedro R. Carvalho on 25/11/2015.
 */
public class BaseHelper {
    private SQLiteOpenHelper dbHelper;
    public SQLiteDatabase database;

    public BaseHelper(Context context)
    {
        dbHelper = new DatabaseHelper(context);
    }

    public void OpenWrite()
    {
        database = dbHelper.getWritableDatabase();
    }

    public void OpenRead()
    {
        database = dbHelper.getReadableDatabase();
    }

    public boolean ResetDatabase(){
        try{
            OpenWrite();
            dbHelper.onUpgrade(database, 0, 0);
            Close();

            return true;
        }
        catch(Exception e){
            return false;
        }
    }

    public void Close()
    {
        dbHelper.close();
    }
}
