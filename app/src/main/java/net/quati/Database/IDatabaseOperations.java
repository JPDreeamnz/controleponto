package net.quati.Database;

import java.util.List;

/**
 * Created by João Pedro R. Carvalho on 25/11/2015.
 */
public interface IDatabaseOperations<Object> {
    public Object Insert(Object instance);
    public boolean Update(Object instance);
    public boolean Delete(int ID);
    public List<Object> findAll();
}
