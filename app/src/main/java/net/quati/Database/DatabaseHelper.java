package net.quati.Database;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by João Pedro R. Carvalho on 25/11/2015.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String DatabaseName = "DB_TimeStampControl";
    private static final int DatabaseVersion = 1;

    public DatabaseHelper(Context context) {
        super(context, DatabaseName, null, DatabaseVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try{
            db.execSQL(DatabaseContract.TimeStamp.TimeStamp_CreateTable);
        }catch (SQLiteException e)
        {
            //treat exception
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            db.execSQL("DROP TABLE IF EXISTS " + DatabaseContract.TimeStamp.Table_Name);
            onCreate(db);
        }catch (SQLiteException e)
        {
            //treat exception
        }
    }
}
