package net.quati.Database;

/**
 * Created by João Pedro R. Carvalho on 25/11/2015.
 */
public class DatabaseContract {

    public static abstract class TimeStamp
    {
        /*
            Regras da Tabela
            StampType: 0 = saida, 1 = entrada
            Status: 0 = inativo, 1 = ativo
         */
        public static final String Table_Name = "TimeStamp";
        public static final String Column_TimeStampID = "TimeStampID";
        public static final String Column_OriginalTimeStampID  = "OriginalTimeStampID";
        public static final String Column_RegisterDateTime = "RegisterDateTIme";
        public static final String Column_StampType = "StampType";
        public static final String Column_Status = "Status";
        public static final String Column_UpdateDateTime = "UpdateDateTime";

        public static final String TimeStamp_CreateTable =
                "CREATE TABLE " + Table_Name + "(" +
                        Column_TimeStampID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        Column_OriginalTimeStampID + " INTEGER NULL, " +
                        Column_StampType + " INTEGER NOT NULL, " +
                        Column_RegisterDateTime + " NUMERIC NOT NULL, " +
                        Column_Status + " NUMERIC NOT NULL, " +
                        Column_UpdateDateTime + " NUMERIC NULL, " +
                        "FOREIGN KEY(" + Column_OriginalTimeStampID + ") REFERENCES " + Table_Name +
                        "(" + Column_TimeStampID + "))";

        public static final String[] TimeStamp_AllColumns = new String[]{
                Column_TimeStampID,
                Column_OriginalTimeStampID,
                Column_RegisterDateTime,
                Column_StampType,
                Column_Status,
                Column_UpdateDateTime
        };

    }

    public static abstract class OvertimeRule
    {
        public static final String Table_Name = "OvertimeRule";
        public static final String Column_OvertimeRuleID = "OvertimeRuleID";
        public static final String Column_AditionalOvertimeRuleID = "ConjunctedOvertimeRuleID";
        public static final String Column_RuleTypeID = "RuleTypeID";
        public static final String Column_FirstPremisse = "FirstPremisse";
        public static final String Column_SecondPremisse = "SecondPremisse";
        public static final String Column_OperatorID = "OperatorID";
        public static final String Column_ConjunctionID = "ConjunctionID";
        public static final String Column_InsertDateTime = "InsertDate";
        public static final String Column_UpdateDateTime = "UpdateDateTime";
        public static final String Column_Status = "Status";
    }

    public static abstract class RuleType
    {
        public static final String Table_Name = "RuleType";
        public static final String Column_RuleTypeID = "RuleTypeID";
        public static final String Column_RuleCode = "RuleCode";
        public static final String Column_RuleName = "RuleName";
        public static final String Column_Status = "Status";
    }

    public static abstract class Operator
    {
        public static final String Table_Name = "Operator";
        public static final String Column_RuleTypeID = "OperatorID";
        public static final String Column_RuleCode = "OperatorCode";
        public static final String Column_RuleName = "OperatorName";
        public static final String Column_Status = "Status";
    }

    public static abstract class Conjunction
    {
        public static final String Table_Name = "Conjunction";
        public static final String Column_RuleTypeID = "ConjunctionID";
        public static final String Column_RuleCode = "ConjunctionCode";
        public static final String Column_RuleName = "ConjunctionName";
        public static final String Column_Status = "Status";
    }
}
