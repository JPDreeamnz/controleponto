package net.quati.Model;

import java.util.Date;

/**
 * Created by João Pedro R. Carvalho on 25/11/2015.
 */
public class TimeStamp {
    public long TimeStampID;
    public Long OriginalTimeStampID;
    public Date RegisterDateTime;
    public int StampType;
    public boolean Status;
    public Date UpdateDateTime;
}
