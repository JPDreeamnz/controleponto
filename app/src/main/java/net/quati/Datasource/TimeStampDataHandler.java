package net.quati.Datasource;

import android.content.ContentValues;
import android.database.Cursor;

import net.quati.Database.DatabaseContract;
import net.quati.Model.TimeStamp;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by João Pedro R. Carvalho on 26/11/2015.
 */
public final class TimeStampDataHandler {

    public static TimeStamp fromCursor(Cursor cursor) throws ParseException {
        TimeStamp timeStamp = null;

        if(cursor.moveToFirst()){
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

            timeStamp = new TimeStamp();
            timeStamp.TimeStampID = cursor.getLong(cursor.getColumnIndex("TimeStampID"));
            timeStamp.OriginalTimeStampID = cursor.getLong(cursor.getColumnIndex("OriginalTimeStampID")) > 0 ?
                    new Long(cursor.getLong(cursor.getColumnIndex("OriginalTimeStampID"))) : null;
            timeStamp.RegisterDateTime = dateFormat.parse(cursor.getString(cursor.getColumnIndex("RegisterDateTime")));
            timeStamp.StampType = cursor.getInt(cursor.getColumnIndex("StampType"));
            timeStamp.Status = (cursor.getInt(cursor.getColumnIndex("Status")) == 1);
            timeStamp.UpdateDateTime = dateFormat.parse(cursor.getString(cursor.getColumnIndex("UpdateDateTime")));
        }

        return timeStamp;
    }

    public static List<TimeStamp> fromCursorToList(Cursor cursor) throws ParseException {
        List<TimeStamp> listaTimeStamp = new ArrayList<TimeStamp>();

        if(cursor.moveToFirst()) {
            do {
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

                TimeStamp timeStamp = new TimeStamp();
                timeStamp.TimeStampID = cursor.getLong(cursor.getColumnIndex(DatabaseContract.TimeStamp.Column_TimeStampID));
                timeStamp.OriginalTimeStampID = cursor.getLong(cursor.getColumnIndex(DatabaseContract.TimeStamp.Column_OriginalTimeStampID)) > 0 ?
                        new Long(cursor.getLong(cursor.getColumnIndex(DatabaseContract.TimeStamp.Column_OriginalTimeStampID))) : null;
                timeStamp.RegisterDateTime = dateFormat.parse(cursor.getString(cursor.getColumnIndex(DatabaseContract.TimeStamp.Column_RegisterDateTime)));
                timeStamp.StampType = cursor.getInt(cursor.getColumnIndex(DatabaseContract.TimeStamp.Column_StampType));
                timeStamp.Status = (cursor.getInt(cursor.getColumnIndex(DatabaseContract.TimeStamp.Column_Status)) == 1);

                String updateDateTime = cursor.getString(cursor.getColumnIndex(DatabaseContract.TimeStamp.Column_UpdateDateTime));
                timeStamp.UpdateDateTime = updateDateTime.isEmpty() ? null : dateFormat.parse(updateDateTime);

                listaTimeStamp.add(timeStamp);
            } while (cursor.moveToNext());
        }

        return listaTimeStamp;
    }

    public static ContentValues toContentValues(TimeStamp instance)
    {
        ContentValues contentValues = new ContentValues();

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");

        contentValues.put("OriginalTimeStampID", instance.OriginalTimeStampID);
        contentValues.put("RegisterDateTime", dateFormat.format(instance.RegisterDateTime));
        contentValues.put("StampType", instance.StampType);
        contentValues.put("Status", instance.Status);
        contentValues.put("UpdateDateTime", (instance.UpdateDateTime != null) ? dateFormat.format(instance.UpdateDateTime) : "");

        return contentValues;
    }
}
