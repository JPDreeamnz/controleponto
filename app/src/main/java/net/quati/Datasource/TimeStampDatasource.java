package net.quati.Datasource;

import android.content.Context;
import net.quati.Database.BaseHelper;
import net.quati.Database.DatabaseContract;
import net.quati.Database.IDatabaseOperations;
import net.quati.Model.TimeStamp;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by João Pedro R. Carvalho on 25/11/2015.
 */
public class TimeStampDatasource extends BaseHelper implements IDatabaseOperations<TimeStamp>{
    public TimeStampDatasource(Context context) {
        super(context);
    }

    @Override
    public TimeStamp Insert(TimeStamp instance) {
        this.OpenWrite();
        instance.TimeStampID = database.insert(DatabaseContract.TimeStamp.Table_Name, null, TimeStampDataHandler.toContentValues(instance));
        this.Close();

        return instance;
    }

    @Override
    public boolean Update(TimeStamp instance) {
        this.OpenWrite();
        int numberOfRows = database.update(DatabaseContract.TimeStamp.Table_Name, TimeStampDataHandler.toContentValues(instance),
                DatabaseContract.TimeStamp.Column_TimeStampID + "=" + instance.TimeStampID, null);
        this.Close();

        return (numberOfRows > 0);
    }

    @Override
    public boolean Delete(int ID) {
        this.OpenWrite();
        int numberOfRows = database.delete(DatabaseContract.TimeStamp.Table_Name, DatabaseContract.TimeStamp.Column_TimeStampID + "=" + ID, null);
        this.Close();

        return (numberOfRows > 0);
    }

    @Override
    public List<TimeStamp> findAll() {
        List<TimeStamp> listTimeStamp = new ArrayList<TimeStamp>();

        try {
            this.OpenRead();
            listTimeStamp = TimeStampDataHandler.fromCursorToList(
                    database.query(DatabaseContract.TimeStamp.Table_Name,
                            DatabaseContract.TimeStamp.TimeStamp_AllColumns,
                            null, null, null, null, null)
            );
            this.Close();
        }
        catch (ParseException e) {
            e.printStackTrace();
        }

        return listTimeStamp;
    }

    public TimeStamp getLastTimeStamp()
    {
        TimeStamp timeStamp = null;
        try {
            this.OpenRead();
            timeStamp = TimeStampDataHandler.fromCursor(
                    database.query(DatabaseContract.TimeStamp.Table_Name,
                            DatabaseContract.TimeStamp.TimeStamp_AllColumns,
                            DatabaseContract.TimeStamp.Column_Status + "=?",
                            new String[]{ "A" },
                            null,
                            null,
                            DatabaseContract.TimeStamp.Column_TimeStampID + " DESC",
                            "1")
            );
            this.Close();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return timeStamp;
    }
}
